# CMAKE generated file: DO NOT EDIT!
# Generated by "Unix Makefiles" Generator, CMake Version 3.8

# Delete rule output on recipe failure.
.DELETE_ON_ERROR:


#=============================================================================
# Special targets provided by cmake.

# Disable implicit rules so canonical targets will work.
.SUFFIXES:


# Remove some rules from gmake that .SUFFIXES does not remove.
SUFFIXES =

.SUFFIXES: .hpux_make_needs_suffix_list


# Suppress display of executed commands.
$(VERBOSE).SILENT:


# A target that is always out of date.
cmake_force:

.PHONY : cmake_force

#=============================================================================
# Set environment variables for the build.

# The shell in which to execute make rules.
SHELL = /bin/sh

# The CMake executable.
CMAKE_COMMAND = /usr/local/Cellar/cmake/3.8.2/bin/cmake

# The command to remove a file.
RM = /usr/local/Cellar/cmake/3.8.2/bin/cmake -E remove -f

# Escaping for special characters.
EQUALS = =

# The top-level source directory on which CMake was run.
CMAKE_SOURCE_DIR = /Users/daniel/Projects/htm_alg

# The top-level build directory on which CMake was run.
CMAKE_BINARY_DIR = /Users/daniel/Projects/htm_alg/build

# Include any dependencies generated for this target.
include CMakeFiles/htm_sgl.dir/depend.make

# Include the progress variables for this target.
include CMakeFiles/htm_sgl.dir/progress.make

# Include the compile flags for this target's objects.
include CMakeFiles/htm_sgl.dir/flags.make

CMakeFiles/htm_sgl.dir/src/htm.cpp.o: CMakeFiles/htm_sgl.dir/flags.make
CMakeFiles/htm_sgl.dir/src/htm.cpp.o: ../src/htm.cpp
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green --progress-dir=/Users/daniel/Projects/htm_alg/build/CMakeFiles --progress-num=$(CMAKE_PROGRESS_1) "Building CXX object CMakeFiles/htm_sgl.dir/src/htm.cpp.o"
	/Applications/Xcode.app/Contents/Developer/Toolchains/XcodeDefault.xctoolchain/usr/bin/c++  $(CXX_DEFINES) $(CXX_INCLUDES) $(CXX_FLAGS) -o CMakeFiles/htm_sgl.dir/src/htm.cpp.o -c /Users/daniel/Projects/htm_alg/src/htm.cpp

CMakeFiles/htm_sgl.dir/src/htm.cpp.i: cmake_force
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green "Preprocessing CXX source to CMakeFiles/htm_sgl.dir/src/htm.cpp.i"
	/Applications/Xcode.app/Contents/Developer/Toolchains/XcodeDefault.xctoolchain/usr/bin/c++ $(CXX_DEFINES) $(CXX_INCLUDES) $(CXX_FLAGS) -E /Users/daniel/Projects/htm_alg/src/htm.cpp > CMakeFiles/htm_sgl.dir/src/htm.cpp.i

CMakeFiles/htm_sgl.dir/src/htm.cpp.s: cmake_force
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green "Compiling CXX source to assembly CMakeFiles/htm_sgl.dir/src/htm.cpp.s"
	/Applications/Xcode.app/Contents/Developer/Toolchains/XcodeDefault.xctoolchain/usr/bin/c++ $(CXX_DEFINES) $(CXX_INCLUDES) $(CXX_FLAGS) -S /Users/daniel/Projects/htm_alg/src/htm.cpp -o CMakeFiles/htm_sgl.dir/src/htm.cpp.s

CMakeFiles/htm_sgl.dir/src/htm.cpp.o.requires:

.PHONY : CMakeFiles/htm_sgl.dir/src/htm.cpp.o.requires

CMakeFiles/htm_sgl.dir/src/htm.cpp.o.provides: CMakeFiles/htm_sgl.dir/src/htm.cpp.o.requires
	$(MAKE) -f CMakeFiles/htm_sgl.dir/build.make CMakeFiles/htm_sgl.dir/src/htm.cpp.o.provides.build
.PHONY : CMakeFiles/htm_sgl.dir/src/htm.cpp.o.provides

CMakeFiles/htm_sgl.dir/src/htm.cpp.o.provides.build: CMakeFiles/htm_sgl.dir/src/htm.cpp.o


# Object files for target htm_sgl
htm_sgl_OBJECTS = \
"CMakeFiles/htm_sgl.dir/src/htm.cpp.o"

# External object files for target htm_sgl
htm_sgl_EXTERNAL_OBJECTS =

../bin/libhtm_sgl.a: CMakeFiles/htm_sgl.dir/src/htm.cpp.o
../bin/libhtm_sgl.a: CMakeFiles/htm_sgl.dir/build.make
../bin/libhtm_sgl.a: CMakeFiles/htm_sgl.dir/link.txt
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green --bold --progress-dir=/Users/daniel/Projects/htm_alg/build/CMakeFiles --progress-num=$(CMAKE_PROGRESS_2) "Linking CXX static library ../bin/libhtm_sgl.a"
	$(CMAKE_COMMAND) -P CMakeFiles/htm_sgl.dir/cmake_clean_target.cmake
	$(CMAKE_COMMAND) -E cmake_link_script CMakeFiles/htm_sgl.dir/link.txt --verbose=$(VERBOSE)

# Rule to build all files generated by this target.
CMakeFiles/htm_sgl.dir/build: ../bin/libhtm_sgl.a

.PHONY : CMakeFiles/htm_sgl.dir/build

CMakeFiles/htm_sgl.dir/requires: CMakeFiles/htm_sgl.dir/src/htm.cpp.o.requires

.PHONY : CMakeFiles/htm_sgl.dir/requires

CMakeFiles/htm_sgl.dir/clean:
	$(CMAKE_COMMAND) -P CMakeFiles/htm_sgl.dir/cmake_clean.cmake
.PHONY : CMakeFiles/htm_sgl.dir/clean

CMakeFiles/htm_sgl.dir/depend:
	cd /Users/daniel/Projects/htm_alg/build && $(CMAKE_COMMAND) -E cmake_depends "Unix Makefiles" /Users/daniel/Projects/htm_alg /Users/daniel/Projects/htm_alg /Users/daniel/Projects/htm_alg/build /Users/daniel/Projects/htm_alg/build /Users/daniel/Projects/htm_alg/build/CMakeFiles/htm_sgl.dir/DependInfo.cmake --color=$(COLOR)
.PHONY : CMakeFiles/htm_sgl.dir/depend

